import { Component, OnInit } from '@angular/core'

import { AppService } from './app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  
  vatNumber: string
  loader: boolean
  result: any
  vatStatus: string
  statusColor: string
  showResult: boolean

  constructor(private service: AppService) {}

  ngOnInit() {
    this.loader = false
    this.showResult = false
    this.statusColor = '#000'
    this.result = {}
  }

  checkNumber() {
    this.showResult = false
    if (this.vatNumber) {
      this.loader = true
      this.service
      .checkVatNumber(this.vatNumber)
      .subscribe(res => {
        this.loader = false
        this.result = res
        this.showResult = true

        if (this.result.Valid) {
          this.vatStatus = 'Valid'
          this.statusColor = '#198C19'
        } else {
          this.vatStatus = 'Invalid'
          this.statusColor = '#FF1919'
        }
      }, err => {
        this.loader = false
        this.showResult = false
        console.log(err)
      })
    } 
  }

  clear() {
    this.result = {}
    this.showResult = false
    this.vatNumber = ''
  }


}
