import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders, HttpParams} from '@angular/common/http'

import { Observable } from 'rxjs'



@Injectable({
  providedIn: 'root'
})
export class AppService {

  constructor(private http: HttpClient) { }

  checkVatNumber(vatNumber: string): Observable<any> {
    const url = 'https://vat.erply.com/numbers'
    const header = new HttpHeaders()
    const params = new HttpParams().set('vatNumber', vatNumber)
    
    return this.http.get(url, { headers: header, params: params })
  }
}
